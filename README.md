README

****

This templates have only been tested on Mozilla Firefox/Ubuntu. 
Your mileage may vary.
I provide no support for these templates.

****

Thanks to subtlepatterns.com for their awesome and free backgrounds.

The LTD LX 5.0 image does not belong to me. Copyright Ford Motor Co.
Used without permission, but with all the love in the world because I love those
cars. Don't ask.
Buy a Ford so they don't sue me.


Licensed under the Apache License, Version 2.0 (the "License"); you may not use this work except in compliance with the License. You may obtain a copy of the License in the LICENSE file, or at:

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.


Also licensed under the GNU WTFPL. 

If you get rich using this, then allow me to tell you that I need a new MacBook Pro. :)

